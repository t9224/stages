# Overview

The basic idea is, that `StageQueue` is holding one (several) `Stage`(s). `Stages` holds several `Timer`.

`StageQueue` can then start all the stages, and they start the timer after each other.

To get a `StageQueue`, use the `StageQueueBuilder`. The builder needs a configuration for the queue with, stage, timer, etc. Additionally, this configuration needs to be provided as a json.

For example:

```json
{
   "type":"stage_queue",
   "name":"Test queue",
   "stages":[
      {
         "type":"stage",
         "name":"Test stage 1",
         "subjects":[
            {
               "type":"timer",
               "hours":0,
               "minutes":0,
               "seconds":1
            },
            {
               "type":"timer",
               "hours":0,
               "minutes":0,
               "seconds":2
            }
         ]
      },
      {
         "type":"stage",
         "name":"Test stage 2",
         "subjects":[
            {
               "type":"timer",
               "hours":0,
               "minutes":0,
               "seconds":3
            },
            {
               "type":"timer",
               "hours":0,
               "minutes":0,
               "seconds":4
            }
         ]
      }
   ]
}
```

The builder will provide a StageQueue with the above settings.

## General flow

```mermaid
flowchart
    StageQueueBuilder --create--> StageQueue
    StageQueueBuilder --create--> Stage
    StageQueueBuilder --create--> Timer
    StageQueueBuilder --attach Stage --> StageQueue
    StageQueueBuilder --attach Timer -->Stage
    StageQueueBuilder --start queue --> StageQueue
    StageQueue --start stage--> Stage
    Stage --start timer--> Timer
```

## Complete UML overview

```mermaid
classDiagram
    direction TB

    class StageQueueBuilder{
        parse_json(String config) StageQueue
    }

    class Timer{
        int hours
        int minutes
        int seconds
        int remaining_seconds
        list observers
        attach_observers() void
        start() void
        update() void
    }

    class InterfaceSubject{
        <<Interface>>
        start()*
        update()*
    }

    class Stage{
        String name
        String status
        attach_timer(Timer timer) Stage
        start_subject() void
        update() void
    }

    class InterfaceObserver{
        <<Interface>>
        update()*
    }

    class StageQueue{
        String name
        list queue
        attach_stage(Stage stage) Stage
        start() void
    }

    Stage ..|> InterfaceObserver : implements
    Timer ..|> InterfaceSubject  : implements
    Timer <.. Stage : injected
    Stage <.. StageQueue : injected

    StageQueueBuilder-->Timer
    StageQueueBuilder-->Stage
    StageQueueBuilder-->StageQueue
```
