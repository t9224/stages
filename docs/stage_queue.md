## Using the stage_queue

```python
from stages.stage_queue import StageQueue
stage_queue = StageQueue("test queue")
```

### You can alo directly attach a stage etc.

```python
from stages.stage import Stage
stage_queue = StageQueue("test queue").attach_stage(Stage("test stage"))
```

## Relationship model between stage, timer, stage_queue

```mermaid
erDiagram
    StageQueue ||--o{ Stage : contains
    Stage ||--o{ Timer : contains

```

## UML overview StageQueue

```mermaid
classDiagram
    direction RL
    class Timer{
        int hours
        int minutes
        int seconds
        int remaining_seconds
        list observers
        attach_observers() void
        start() void
        update() void
    }

    class InterfaceSubject{
        <<Interface>>
        start()*
        update()*
    }

    class Stage{
        String name
        String status
        attach_timer(Timer timer) Stage
        start_subject() void
        update() void
    }

    class InterfaceObserver{
        <<Interface>>
        update()*
    }

    class StageQueue{
        String name
        list queue
        attach_stage(Stage stage) Stage
        start() void
    }

    Stage ..|> InterfaceObserver : implements
    Timer ..|> InterfaceSubject  : implements
    Timer <.. Stage : injected
    Stage <.. StageQueue : injected
```

## Class overview

::: stages.stage_queue
