## Using the StageQueueBuilder

```python
from stages.stage_queue_builder import StageQueueBuilder
queue = StageQueueBuilder().parse_json()
```

## UML overview StageQueueBuilder

```mermaid
classDiagram
    class StageQueueBuilder{
        parse_json(String config) StageQueue
    }

    StageQueueBuilder-->Timer
    StageQueueBuilder-->Stage
    StageQueueBuilder-->StageQueue

```

## Class overview
::: stages.stage_queue_builder
