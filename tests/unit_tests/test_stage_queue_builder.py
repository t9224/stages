import pytest
import json
import copy

from stages.timer import Timer
from stages.stage_queue_builder import StageQueueBuilder


@pytest.fixture
def builder():
    builder = StageQueueBuilder()
    return builder


@pytest.fixture
def timer_dict():
    timer = {
        "type": "timer",
        "hours": 0,
        "minutes": 0,
        "seconds": 1,
    }
    return timer


@pytest.fixture
def stage_dict(timer_dict):
    stage = {
        "type": "stage",
        "name": "Test stage",
        "subjects": [copy.deepcopy(timer_dict), copy.deepcopy(timer_dict)],
    }
    stage["subjects"][1]["seconds"] = 2
    return stage


@pytest.fixture
def queue_dict(stage_dict):
    queue = {
        "type": "stage_queue",
        "name": "Test queue",
        "stages": [copy.deepcopy(stage_dict), copy.deepcopy(stage_dict)],
    }
    queue["stages"][0]["subjects"][0]["seconds"] = 1
    queue["stages"][0]["subjects"][1]["seconds"] = 2
    queue["stages"][1]["subjects"][0]["seconds"] = 3
    queue["stages"][1]["subjects"][1]["seconds"] = 4

    return queue


def test_parse_timer_json(timer_dict, builder):
    timer = builder._create_timer(timer_dict)
    assert timer.seconds == 1


def test_parse_stage_and_timer(stage_dict, builder):
    stage = builder._create_stage(stage_dict)
    assert stage.name == "Test stage"
    assert stage.subjects[0].seconds == 1
    assert stage.subjects[1].seconds == 2


def test_parse_stages_and_timer_from_string(queue_dict, builder):
    stage_queue = builder._create_stage_queue(queue_dict)

    assert stage_queue.name == "Test queue"
    assert stage_queue.queue[0].subjects[0].seconds == 1
    assert stage_queue.queue[-1].subjects[-1].seconds == 4


def test_parse_from_json(builder):
    json = '{"type": "stage_queue", "name": "Test queue", "stages": [{"type": "stage", "name": "Test stage 1", "subjects": [{"type": "timer", "hours": 0, "minutes": 0, "seconds": 1}, {"type": "timer", "hours": 0, "minutes": 0, "seconds": 2}]}, {"type": "stage", "name": "Test stage 2", "subjects": [{"type": "timer", "hours": 0, "minutes": 0, "seconds": 3}, {"type": "timer", "hours": 0, "minutes": 0, "seconds": 4}]}]}'

    stage_queue = builder.parse_json(json)

    assert stage_queue.name == "Test queue"
    assert stage_queue.queue[0].subjects[0].seconds == 1
    assert stage_queue.queue[-1].subjects[-1].seconds == 4
