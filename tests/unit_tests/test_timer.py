import pytest
import logging
import pathlib

from unittest.mock import patch

from stages.timer import Timer
from stages.stage import Stage
from stages.interface_observer import InterfaceObserver

import threading


@pytest.fixture
def test_timer():
    return Timer(hours=0, minutes=0, seconds=1)


def test_define_timer():
    timer = Timer(hours=0, minutes=0, seconds=1)

    assert timer.hours == 0
    assert timer.minutes == 0
    assert timer.seconds == 1


def test_set_hours_minutes_seconds(test_timer):
    with pytest.raises(Exception) as e:
        test_timer.hours = 1

    assert "Cannot change" in str(e)

    with pytest.raises(Exception) as e:
        test_timer.minutes = 1

    assert "Cannot change" in str(e)

    with pytest.raises(Exception) as e:
        test_timer.seconds = 1

    assert "Cannot change" in str(e)


def test_create_stage_and_timer_at_once():
    stage = Stage("test stage and timer").attach_timer(Timer(0, 0, 0))
    assert stage.name == "test stage and timer"


def test_total_seconds_countdown():
    timer = Timer(hours=1, minutes=1, seconds=1)
    assert timer.remaining_seconds == 3661

    timer = Timer(hours=2, minutes=2, seconds=2)
    assert timer.remaining_seconds == 7322


def test_set_remaining_seconds(test_timer):
    with pytest.raises(Exception):
        test_timer.remaining_seconds = 1


@patch("time.sleep", return_value=None)
def test_start_timer(patched_time_sleep, test_timer):
    test_timer.start()
    assert test_timer.remaining_seconds == 0


@patch("time.sleep", return_value=None)
def test_restart_timer(patched_time_sleep, test_timer):
    assert test_timer.remaining_seconds == 1
    test_timer.start()
    assert test_timer.remaining_seconds == 0
    test_timer.start()
    assert test_timer.remaining_seconds == 0


def test_add_observer(test_timer):
    test_timer.attach_observer(Stage(name="Test stage"))
    assert issubclass(type(test_timer.observers[0]), InterfaceObserver)


def test_get_id(test_timer):
    assert test_timer.id != None


def test_timer_break():
    timer = Timer(0, 0, 1)
    x = threading.Thread(target=timer.start, args=(), daemon=True)
    x.start()
    timer.stop()
    x.join()
    assert timer.remaining_seconds >= 0.1
