import time
import pytest

from stages.stage_queue import StageQueue
from stages.stage import Stage
from stages.timer import Timer


def test_several_stages():
    queue = StageQueue("Test queue")
    queue.attach_stage(
        Stage("test stage")
        .attach_timer(Timer(0, 0, 3))
        .attach_timer(Timer(0, 0, 50))
        .attach_timer(Timer(0, 0, 30))
    )
    queue.attach_stage(
        Stage("test stage 2")
        .attach_timer(Timer(0, 0, 50))
        .attach_timer(Timer(0, 0, 50))
    )

    total_seconds: int = int()

    for stage in queue.queue:
        for timer in stage.subjects:
            total_seconds += timer.remaining_seconds

    start = time.perf_counter()
    queue.start()
    stop = time.perf_counter()

    assert (stop - start) >= total_seconds and (stop - start) < total_seconds + 1
