import threading
import pytest
import time

from stages.timer import Timer


@pytest.fixture
def test_timer() -> Timer:
    return Timer(0, 0, 10)


@pytest.mark.parametrize(
    "length", [(0, 0, 0), (0, 0, 1), (0, 0, 10), (0, 1, 0), (0, 1, 1)]
)
def test_timer_length(length):

    timer = Timer(*length)
    total_seconds = timer.remaining_seconds

    start = time.perf_counter()
    timer.start()

    stop = time.perf_counter()

    assert (stop - start) >= total_seconds and (stop - start) < total_seconds + 0.5


def test_update_information(test_timer):
    id = test_timer.id
    test_timer.start()
    assert test_timer.last_update == f"{id} - 0.0"


def test_restart_after_stop(test_timer):
    x = threading.Thread(target=test_timer.start, args=(), daemon=True)

    assert 10 >= test_timer.remaining_seconds > 0

    x.start()
    time.sleep(5)
    assert 5.5 >= test_timer.remaining_seconds > 0

    test_timer.stop()
    test_timer.start()
    time.sleep(2)

    assert 3.5 >= test_timer.remaining_seconds >= 0
    x.join()
